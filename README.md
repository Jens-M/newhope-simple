# README #

# A Golang implementation of the NewHope-Simple key exchange scheme #

This repository contains the implementation of the Post-quantum key exchange scheme __NewHope-Simple__, proposed
in the paper [NewHope without reconciliation](https://cryptojedi.org/papers/index.shtml) by Alkim, Ducas, Poeppelmann and Schwabe .
The Golang translation is based on the original C reference from [Post-quantum key exchange – a new hope](https://cryptojedi.org/crypto/#newhope),
and can be found here [github](https://github.com/tpoeppelmann/newhope).