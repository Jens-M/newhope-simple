package main

import (
	"fmt"
	"bytes"
)

const NTESTS = 1000


func main() {
	var alice Alice
	var bob Bob

	k := 0
	for i := 0; i < NTESTS; i++ {

		newHopeSimpleKeygen(&alice)
		newHopeSimpleSharedB(alice.sendA[:], &bob)
		newNopeSimpleSharedA(bob.sendB[:], &alice)

		if bytes.Equal(alice.keyA[:], bob.keyB[:]) {
			k++
			fmt.Println("Key Alice: ", alice.keyA)
			fmt.Println("Key Bob  : ", bob.keyB)
		}

		alice.clearSelf()
		bob.clearSelf()
	}
	fmt.Println("number of key matches: ", k)
}

// #		below code is for testing with testvectors
// #		usage:
// #		1. in main.go uncomment lint 40 - 58, comment 11 - 32
// #		2. in random_byte_bits.go uncomment line 38 - 100, comment line 8 - 14
// #		3. in newhope_simple.go uncomment line 55, comment line 54

/*
func main() {

	var alice Alice
	var bob Bob

	for i := 0; i < NTESTS; i++ {
		newHopeSimpleKeygen(&alice)
		fmt.Printf("SendA, [%02x]\n",alice.sendA)

		newHopeSimpleSharedB(alice.sendA[:], &bob)
		fmt.Printf("SendB, [%02x]\n",bob.sendB)

		newNopeSimpleSharedA(bob.sendB[:], &alice)
		fmt.Printf("KeyA, [%02x]\n",alice.keyA)
		fmt.Printf("KeyB, [%02x]\n\n",bob.keyB)
	}
}
*/



