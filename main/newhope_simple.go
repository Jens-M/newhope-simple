package main

import (
	"golang.org/x/crypto/sha3"
)

func newHopeSimpleKeygen(alice *Alice) {
	var seed, noiseSeed [NEWHOPE_SEEDBYTE]byte
	var a_hat, b_hat, e, temp Poly

	getRandomBytes(seed[:], NEWHOPE_SEEDBYTE)
	getRandomBytes(noiseSeed[:], NEWHOPE_SEEDBYTE)

	a_hat.uniform(seed)

	alice.s_hat.getNoise(noiseSeed, 0)
	e.getNoise(noiseSeed, 1)

	alice.s_hat.ntt()

	e.ntt()
	temp.pointwise(&alice.s_hat, &a_hat)
	b_hat.add(&temp, &e)

	encodeA(alice.sendA[:], &b_hat, seed[:])

	a_hat.clearCoeffs()
	b_hat.clearCoeffs()
	e.clearCoeffs()
	temp.clearCoeffs()
	clearByteArray(seed[:])
	clearByteArray(noiseSeed[:])
}

func newHopeSimpleSharedB(received []byte, bob *Bob) {
	var seed, noiseSeed, vp [NEWHOPE_SEEDBYTE]byte
	var a_hat, b_hat, t_hat, u_hat, c_bar, ep, epp, k, temp Poly

	getRandomBytes(noiseSeed[:], NEWHOPE_SEEDBYTE)

	t_hat.getNoise(noiseSeed, 0)
	ep.getNoise(noiseSeed, 1)
	epp.getNoise(noiseSeed, 2)

	decodeA(&b_hat, &seed, received[:])
	a_hat.uniform(seed)

	t_hat.ntt()

	ep.ntt()
	temp.pointwise(&t_hat, &a_hat)
	u_hat.add(&temp, &ep)

	getRandomBits(&vp)
	//getRandomBytes(vp[:], NEWHOPE_SEEDBYTE)
	vp = sha3.Sum256(vp[:])

	nhsEncode(&k, vp[:])

	temp.pointwise(&t_hat, &b_hat)
	temp.invNtt()
	temp.add(&temp, &epp)
	c_bar.add(&temp, &k)
	c_bar = nhsCompress(&c_bar)

	encodeB(bob.sendB[:], &u_hat, &c_bar)
	bob.keyB = sha3.Sum256(vp[:]) // mu

	a_hat.clearCoeffs()
	b_hat.clearCoeffs()
	t_hat.clearCoeffs()
	u_hat.clearCoeffs()
	c_bar.clearCoeffs()
	ep.clearCoeffs()
	epp.clearCoeffs()
	k.clearCoeffs()
	temp.clearCoeffs()
	clearByteArray(seed[:])
	clearByteArray(noiseSeed[:])
}

func newNopeSimpleSharedA(received []byte, alice *Alice) {
	var u_hat, cp, kp, temp Poly
	var vp [NEWHOPE_SEEDBYTE]byte

	decodeB(&u_hat, &cp, received[:])

	cp = nhsDecompress(&cp)
	temp.pointwise(&alice.s_hat, &u_hat)

	temp.invNtt()
	kp.sub(&temp, &cp)

	nhsDecode(&kp, &vp)
	alice.keyA = sha3.Sum256(vp[:]) //mu

	u_hat.clearCoeffs()
	cp.clearCoeffs()
	kp.clearCoeffs()
	temp.clearCoeffs()
	clearByteArray(vp[:])
}