package main

func nhsCompress(c *Poly) Poly {
	var c_bar Poly

	for i := 0; i < PARAM_N; i++ {

		temp := uint32(c.coeffs[i] + 2*PARAM_Q)
		temp *= 8
		temp /= PARAM_Q
		temp = uint32(float32(temp) + 0.5)
		temp = temp % 8

		c_bar.coeffs[i] = uint16(temp)
	}
	return c_bar
}

func nhsDecompress(c_bar *Poly) Poly {
	var c_ Poly

	for i := 0; i < PARAM_N; i ++ {
		temp := uint32(c_bar.coeffs[i]) * PARAM_Q
		temp /= 8
		c_.coeffs[i] = uint16(float32(temp) + 0.5)
	}
	return c_
}

func nhsEncode(k *Poly, v []byte) {
	bits := byteToBit(v)

	for i := uint(0); i < 32; i++ {
		for j := uint(0); j < 8; j++ {
			k.coeffs[8*i+j] = uint16(bits[8*i+j]) * (PARAM_Q_DIV_2)
			k.coeffs[8*i+j+256] = uint16(bits[8*i+j]) * (PARAM_Q_DIV_2)
			k.coeffs[8*i+j+512] = uint16(bits[8*i+j]) * (PARAM_Q_DIV_2)
			k.coeffs[8*i+j+768] = uint16(bits[8*i+j]) * (PARAM_Q_DIV_2)
		}
	}

	clearByteArray(bits[:])
}

func nhsDecode(k *Poly, v *[NEWHOPE_SEEDBYTE]byte) {
	var bits [256]byte
	for i := uint(0); i < 256; i++ {

		t := abs(k.coeffs[i+0])
		t += abs(k.coeffs[i+256])
		t += abs(k.coeffs[i+512])
		t += abs(k.coeffs[i+768])

		if t < PARAM_Q {
			bits[i] = 1
		} else {
			bits[i] = 0
		}
	}
	temp := bitToByte(bits[:])

	for i := 0; i < 32; i++ {
		v[i] = temp[i]
	}

	clearByteArray(bits[:])
	clearByteArray(temp[:])
}

func abs(x uint16) uint16 {
	r := x - PARAM_Q_DIV_2
	if r < 0 {
		r = uint16(int(r) * (-1))
	}
	return uint16(r)
}

func encodeA(r []byte, b_hat *Poly, seed []byte) {
	b_hat.toBytes(r)

	for i := 0; i < NEWHOPE_SEEDBYTE; i++ {
		r[POLY_BYTES+i] = seed[i]
	}
}

func decodeA(b_hat *Poly, seed *[NEWHOPE_SEEDBYTE]byte, r []byte) {
	b_hat.fromBytes(r)

	for i := range seed {
		seed[i] = r[POLY_BYTES+i]
	}
}

func encodeB(r []byte, u_hat *Poly, c_bar *Poly) {
	u_hat.toBytes(r)

	var temp [8]uint16
	for i, j := 0, 0; i < PARAM_N; i, j = i+8, j+3 {
		for k := 0; k < 8; k++ {
			temp[k] = c_bar.coeffs[k+i]
		}
		/*---------------------------------------------------------
		*
		*	Distribution of 8 coefficients into the 3 bytes each coefficient is in range c_i in {0,1,...,7}
		*	The largest number is 7 and is binary 111. This allows a distribution of the 8 coefficients to 3 bytes.
		* 	A possible solution which is implemented is shown below.
		*
		*	-----------------
		*	|7|7|2|2|2|1|1|1|    Byte 1 consists of the 1st, 2nd, and 2 bits of the 7th coefficients
		*	-----------------
		*
		*	-----------------
		*	|8|8|4|4|4|3|3|3|	Byte 2 consists of the 3rd, 4th, and 2 bits of the 8th coefficients
		*	-----------------
		*
		*	-----------------
		*	|8|7|6|6|6|5|5|5|	Byte 3 consists of the 5th, 6th, 1 bit of the 7th, and 1 bit of the 8th coefficients
		*	-----------------
		*
		 */

		r[POLY_BYTES + j] = byte(temp[0])
		r[POLY_BYTES + j] |= byte(temp[1] << 3)

		r[POLY_BYTES + j + 1] = byte(temp[2])
		r[POLY_BYTES + j + 1] |= byte(temp[3] << 3)

		r[POLY_BYTES + j + 2] = byte(temp[4])
		r[POLY_BYTES + j + 2] |= byte(temp[5] << 3)

		r[POLY_BYTES + j] |= byte(temp[6] >> 1) << 6
		r[POLY_BYTES + j + 2] |= byte((temp[6] & 1) << 6)

		r[POLY_BYTES + j + 1] |= byte(temp[7] >> 1) << 6
		r[POLY_BYTES + j + 2] |= byte((temp[7] & 1) << 7)
	}
}

func decodeB(u_hat *Poly, c_bar *Poly, r []byte) {
	u_hat.fromBytes(r)

	for i, j := 0, 0; i < PARAM_N; i, j = i+8, j+3 {

		c_bar.coeffs[i+0] = uint16(r[POLY_BYTES + j] & 7)
		c_bar.coeffs[i+1] = uint16(r[POLY_BYTES + j] >> 3) & 7
		c_bar.coeffs[i+2] = uint16(r[POLY_BYTES + j + 1] & 7)
		c_bar.coeffs[i+3] = uint16(r[POLY_BYTES + j + 1] >> 3) & 7
		c_bar.coeffs[i+4] = uint16(r[POLY_BYTES + j + 2] & 7)
		c_bar.coeffs[i+5] = uint16(r[POLY_BYTES + j + 2] >> 3) & 7
		c_bar.coeffs[i+6] = uint16((uint16(r[POLY_BYTES + j] >> 6) & 3) << 1)
		c_bar.coeffs[i+6] |= uint16(r[POLY_BYTES + j + 2] >> 6) & 1
		c_bar.coeffs[i+7] = uint16((uint16(r[POLY_BYTES + j + 1] >> 6) & 3) << 1)
		c_bar.coeffs[i+7] |= uint16(r[POLY_BYTES + j + 2] >> 7) & 1
	}
}