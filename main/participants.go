package main

type Alice struct {
	s_hat Poly
	sendA [NEWHOPE_SENDABYTES]byte
	keyA [NHS_SECRET_SIZE]byte
}

func (alice *Alice) clearSelf() {
	alice.s_hat.clearCoeffs()
	clearByteArray(alice.sendA[:])
	clearByteArray(alice.keyA[:])
}

type Bob struct {
	sendB [NEWHOPE_SENDBBYTES]byte
	keyB [NHS_SECRET_SIZE]byte
}

func (bob *Bob) clearSelf() {
	clearByteArray(bob.sendB[:])
	clearByteArray(bob.keyB[:])
}

func clearByteArray(x []byte){
	for i := 0; i < len(x); i++ {
		x[i] = 0
	}
}