package main

import (
	"golang.org/x/crypto/sha3"
	"github.com/codahale/chacha20"
)

type Poly struct {
	coeffs [PARAM_N]uint16
}

func init() {
	if PARAM_K != 16 {
		panic("poly_getnoise in poly.go only supports k=16")
	}
}

func (p *Poly) fromBytes(src_poly []byte) {
	for i := 0; i < PARAM_N_DIV_4; i++ {
		p.coeffs[4*i+0] = uint16(src_poly[7*i+0]) | ((uint16(src_poly[7*i+1]) & 0x3f) << 8)
		p.coeffs[4*i+1] = (uint16(src_poly[7*i+1]) >> 6) | (uint16(src_poly[7*i+2]) << 2) | ((uint16(src_poly[7*i+3]) & 0x0f) << 10)
		p.coeffs[4*i+2] = (uint16(src_poly[7*i+3]) >> 4) | (uint16(src_poly[7*i+4]) << 4) | ((uint16(src_poly[7*i+5]) & 0x03) << 12)
		p.coeffs[4*i+3] = (uint16(src_poly[7*i+5]) >> 2) | (uint16(src_poly[7*i+6]) << 6)
	}
}

func (p *Poly) toBytes(r []byte) {
	for i := 0; i < PARAM_N_DIV_4; i++ {
		// Make sure that coefficients have only 14 bits.
		t0 := barrettReduce(p.coeffs[4*i+0])
		t1 := barrettReduce(p.coeffs[4*i+1])
		t2 := barrettReduce(p.coeffs[4*i+2])
		t3 := barrettReduce(p.coeffs[4*i+3])

		// Make sure that coefficients are in [0,q]
		m := t0 - PARAM_Q
		c := int16(m)
		c >>= 15
		t0 = m ^ ((t0 ^ m) & uint16(c))

		m = t1 - PARAM_Q
		c = int16(m)
		c >>= 15
		t1 = m ^ ((t1 ^ m) & uint16(c))

		m = t2 - PARAM_Q
		c = int16(m)
		c >>= 15
		t2 = m ^ ((t2 ^ m) & uint16(c))

		m = t3 - PARAM_Q
		c = int16(m)
		c >>= 15
		t3 = m ^ ((t3 ^ m) & uint16(c))

		r[7*i+0] = byte(t0 & 0xff)
		r[7*i+1] = byte(t0>>8) | byte(t1<<6)
		r[7*i+2] = byte(t1 >> 2)
		r[7*i+3] = byte(t1>>10) | byte(t2<<4)
		r[7*i+4] = byte(t2 >> 4)
		r[7*i+5] = byte(t2>>12) | byte(t3<<2)
		r[7*i+6] = byte(t3 >> 6)
	}
}

func (p *Poly) uniform(seed [NEWHOPE_SEEDBYTE]byte) {
	nBlocks := 16
	var buf [SHAKE_128_RATE * 16]byte

	hash_shake_128 := sha3.NewShake128()
	hash_shake_128.Write(seed[:])
	hash_shake_128.Read(buf[:])

	for ctr, pos := 0, 0; ctr < PARAM_N; {
		val := uint16(buf[pos]) | uint16(buf[pos+1])&0x3fff

		if val < PARAM_Q {
			p.coeffs[ctr] = val
			ctr++
		}
		pos += 2
		if pos > SHAKE_128_RATE*nBlocks-2 {
			nBlocks = 1
			hash_shake_128.Read(buf[:SHAKE_128_RATE])
			pos = 0
		}
	}
}

func (p *Poly) getNoise(seed [NEWHOPE_SEEDBYTE]byte, nonce byte) {

	buf := make([]byte, PARAM_N)
	var n [24]byte
	n[0] = nonce

	cipher, err := chacha20.NewXChaChaWithRounds(seed[:], n[:], 20)
	if err != nil {
		panic(err)
	}

	for k := 0; k < 4; k++ {
		cipher.XORKeyStream(buf, buf)
		for i := 0; i < PARAM_N/4; i++ {
			t := uint32(buf[i])
			d := uint32(0)
			for j := uint16(0); j < 8; j++ {
				d = (t >> j) & 0x01010101
			}

			a := ((d >> 8) & 0xff) + (d & 0xff)
			b := (d >> 24) + ((d >> 16) & 0xff)
			p.coeffs[i+k*PARAM_N_DIV_4] = PARAM_Q + uint16(a) - uint16(b)
		}
	}
}

func (p *Poly) pointwise(a, b *Poly) {
	for i := range p.coeffs {
		t := montgomeryReduce(3186 * uint32(b.coeffs[i]))               // t is now in Montgomery domain
		p.coeffs[i] = montgomeryReduce(uint32(a.coeffs[i]) * uint32(t)) // p.coeffs[i] is back in normal domain
	}
}

func (p *Poly) add(a, b *Poly) {
	for i := range p.coeffs {
		p.coeffs[i] = barrettReduce(a.coeffs[i] + b.coeffs[i])
	}
}

func (p *Poly) sub(a, b *Poly) {
	for i := range p.coeffs {
		p.coeffs[i] = barrettReduce(a.coeffs[i] + 2*PARAM_Q - b.coeffs[i])
	}
}

func (p *Poly) ntt() {
	p.mulCoefficients(&psisBitrevMontgomery)
	ntt(&p.coeffs, &omegasMontgomery)
}

func (p *Poly) invNtt() {
	p.bitrev()
	ntt(&p.coeffs, &omegasInvMontgomery)
	p.mulCoefficients(&psisInvMontgomery)
}

func (p *Poly) clearCoeffs(){
	for i := 0 ; i < PARAM_N; i++ {
		p.coeffs[i] = 0
	}
}