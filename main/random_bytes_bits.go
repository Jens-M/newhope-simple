package main

import (
	"crypto/rand"
	"math/big"
)

func random(max int) byte {
	nBig, err := rand.Int(rand.Reader, big.NewInt(int64(max)))
	if err != nil {
		panic(err)
	}
	return byte(int(nBig.Uint64()) % max)
}

func getRandomBytes(bytes []byte, length int) {

	for i := 0; i < length; i++ {
		bytes[i] = random(256)
	}
}

func getRandomBits(bytes *[NEWHOPE_SEEDBYTE]byte) {
	var bits [RANDOM_BIT_SIZE]byte

	for i := 0; i < RANDOM_BIT_SIZE; i++ {
		bits[i] = random(2)
	}
	temp := bitToByte(bits[:])
	for i := 0; i < NEWHOPE_SEEDBYTE; i++ {
		bytes[i] = temp[i]
	}
}


// below code is for testing with testvectors

/*
var seed = [32]uint32{
	3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 8, 9, 7, 9, 3, 2, 3, 8, 4, 6, 2, 6, 4, 3, 3, 8, 3, 2, 7, 9, 5,
}
var in [12]uint32
var out[8]uint32
var outleft = 0

func surf() {
	var t [12]uint32
	var sum uint32

	for i, v := range in {
		t[i] = v ^ seed[12+i]
	}
	for i := range out {
		out[i] = seed[24+i]
	}
	x := t[11]
	ROTATE := func(x uint32, b uint) uint32 {
		return (((x) << (b)) | ((x) >> (32 - (b))))
	}
	MUSH := func(i int, b uint) {
		t[i] += (((x ^ seed[i]) + sum) ^ ROTATE(x, b))
		x = t[i]
	}
	for loop := 0; loop < 2; loop++ {
		for rr := 0; rr < 16; rr++ {
			sum += 0x9e3779b9
			MUSH(0, 5); MUSH(1, 7); MUSH(2, 9); MUSH(3, 13)
			MUSH(4, 5); MUSH(5, 7); MUSH(6, 9); MUSH(7, 13)
			MUSH(8, 5); MUSH(9, 7); MUSH(10, 9); MUSH(11, 13)
		}
		for i := range out {
			out[i] ^= t[i+4]
		}
	}
}

func getRandomBytes(x []byte, len int){
	length := len
	for length > 0 {
		if outleft == 0 {
			in[0]++
			if in[0] == 0 {
				in[1]++
				if in[1] == 0 {
					in[2]++
					if in[2] == 0 {
						in[3]++
					}
				}
			}
			surf()
			outleft = 8
		}
		outleft--
		x[0] = byte(out[outleft])
		x = x[1:]
		length--
	}
}
*/