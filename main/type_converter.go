package main

func byteToBit(msg_byte []byte) []byte {
	var msg_bit [256]byte

	for i := 0; i < len(msg_byte); i++ {
		temp := msg_byte[i]
		for j := 8; j > 0; j-- {
			msg_bit[j-1+i*8] = temp % 2
			temp /= 2
		}
	}

	return msg_bit[:]
}

func bitToByte(msg_bit []byte) [NEWHOPE_SEEDBYTE]byte {
	var msg_byte [NEWHOPE_SEEDBYTE]byte
	var pot = [8]byte{1, 2, 4, 8, 16, 32, 64, 128}
	for i := 0; i < 32; i++ {
		var temp byte
		for j := 0; j < 8; j++ {
			temp += msg_bit[7-j+8*i] * pot[j]
		}
		msg_byte[i] = temp
		temp = 0
	}
	return msg_byte
}